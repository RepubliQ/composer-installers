# A Multi-Framework [Composer](http://getcomposer.org) Library Installer

This is for PHP package authors to require in their `composer.json`. It will
install their package to the correct location based on the specified package
type.

The goal of `installers` is to be a simple package type to install path map.
Users can also customize the install path per package and package authors can
modify the package name upon installing.

`installers` isn't intended on replacing all custom installers. If your
package requires special installation handling then by all means, create a
custom installer to handle it.

**Natively Supported Frameworks**:

The following frameworks natively work with Composer and will be
installed to the default `vendor` directory. `composer/installers`
is not needed to install packages with these frameworks:

* Aura
* Symfony2
* Yii
* Yii2

**Current Supported Package Types**:

> Stable types are marked as **bold**, this means that installation paths
> for those type will not be changed. Any adjustment for those types would
> require creation of brand new type that will cover required changes.

| Framework    | Types
| ---------    | -----
| Aimeos       | `aimeos-extension`
| Asgard       | `asgard-module` `asgard-theme`
| AGL          | `agl-module`
| AnnotateCms  | `annotatecms-module` `annotatecms-component` `annotatecms-service`
| Bitrix       | `bitrix-module` `bitrix-component` `bitrix-theme`
| CakePHP 2+   | **`cakephp-plugin`**
| Chef         | `chef-cookbook` `chef-role`
| CCFramework  | `ccframework-ship` `ccframework-theme`
| CodeIgniter  | `codeigniter-library` `codeigniter-third-party` `codeigniter-module`
| concrete5    | `concrete5-block` `concrete5-package` `concrete5-theme` `concrete5-update`
| Craft        | `craft-plugin`
| Croogo       | `croogo-plugin` `croogo-theme`
| DokuWiki     | `dokuwiki-plugin` `dokuwiki-template`
| Dolibarr     | `dolibarr-module`
| Drupal       | `drupal-module` `drupal-theme` `drupal-library` `drupal-profile` `drupal-drush`
| Elgg         | `elgg-plugin`
| FuelPHP v1.x | `fuel-module` `fuel-package` `fuel-theme`
| FuelPHP v2.x | `fuelphp-component`
| Grav         | `grav-plugin` `grav-theme`
| Hurad        | `hurad-plugin` `hurad-theme`
| Joomla       | `joomla-component` `joomla-module` `joomla-template` `joomla-plugin` `joomla-library`
| Kirby        | **`kirby-plugin`**
| Kohana       | **`kohana-module`**
| Laravel      | `laravel-library`
| Lithium      | **`lithium-library` `lithium-source`**
| Magento      | `magento-library` `magento-skin` `magento-theme`
| Mako         | `mako-package`
| MODX Evo     | `modxevo-snippet` `modxevo-plugin` `modxevo-module` `modxevo-template` `modxevo-lib`
| MediaWiki    | `mediawiki-extension`
| October      | **`october-module` `october-plugin` `october-theme`**
| OXID         | `oxid-module` `oxid-theme` `oxid-out`
| MODULEWork   | `modulework-module`
| Moodle       | `moodle-*` (Please [check source](https://raw.githubusercontent.com/composer/installers/master/src/Composer/Installers/MoodleInstaller.php) for all supported types)
| Piwik        | `piwik-plugin`
| phpBB        | `phpbb-extension` `phpbb-style` `phpbb-language`
| Pimcore      | `pimcore-plugin`
| PPI          | **`ppi-module`**
| Puppet       | `puppet-module`
| REDAXO       | `redaxo-addon`
| Roundcube    | `roundcube-plugin`
| shopware     | `shopware-backend-plugin` `shopware-core-plugin` `shopware-frontend-plugin` `shopware-theme`
| SilverStripe | `silverstripe-module` `silverstripe-theme`
| SMF          | `smf-module` `smf-theme`
| symfony1     | **`symfony1-plugin`**
| Tusk         | `tusk-task` `tusk-command` `tusk-asset`
| TYPO3 Flow   | `typo3-flow-package` `typo3-flow-framework` `typo3-flow-plugin` `typo3-flow-site` `typo3-flow-boilerplate` `typo3-flow-build`
| TYPO3 CMS    | `typo3-cms-extension`
| Wolf CMS     | `wolfcms-plugin`
| WordPress    | `wordpress-plugin` `wordpress-theme` `wordpress-muplugin`
| Zend         | `zend-library` `zend-extra` `zend-module`
| Zikula       | `zikula-module` `zikula-theme`
| Prestashop   | `prestashop-module` `prestashop-theme`

## Example `composer.json` File

This is an example for a CakePHP plugin. The only important parts to set in your
composer.json file are `"type": "cakephp-plugin"` which describes what your
package is and `"require": { "composer/installers": "~1.0" }` which tells composer
to load the custom installers.

```json
{
    "name": "you/ftp",
    "type": "cakephp-plugin",
    "require": {
        "composer/installers": "~1.0"
    }
}
```

This would install your package to the `Plugin/Ftp/` folder of a CakePHP app
when a user runs `php composer.phar install`.

So submit your packages to [packagist.org](http://packagist.org)!

## Custom Install Paths

If you are consuming a package that uses the `composer/installers` you can
override the install path with the following extra in your `composer.json`:

```json
{
    "extra": {
        "installer-paths": {
            "your/custom/path/{$name}/": ["shama/ftp", "vendor/package"]
        }
    }
}
```

A package type can have a custom installation path with a `type:` prefix.

``` json
{
    "extra": {
        "installer-paths": {
            "your/custom/path/{$name}/": ["type:wordpress-plugin"]
        }
    }
}
```

This would use your custom path for each of the listed packages. The available
variables to use in your paths are: `{$name}`, `{$vendor}`, `{$type}`.

## Custom Install Names

If you're a package author and need your package to be named differently when
installed consider using the `installer-name` extra.

For example you have a package named `shama/cakephp-ftp` with the type
`cakephp-plugin`. Installing with `composer/installers` would install to the
path `Plugin/CakephpFtp`. Due to the strict naming conventions, you as a
package author actually need the package to be named and installed to
`Plugin/Ftp`. Using the following config within your **package** `composer.json`
will allow this:

```json
{
    "name": "shama/cakephp-ftp",
    "type": "cakephp-plugin",
    "extra": {
        "installer-name": "Ftp"
    }
}
```

Please note the name entered into `installer-name` will be the final and will
not be inflected.

### Dynamic package types

You can also specify where you want custom packages installed like this:

```json
{
    "extra": {
        "installer-paths": {
            "packages/{$name}": [
                "type:custom-type"
            ]
        }
    }
}
```
